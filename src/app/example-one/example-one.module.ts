import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ExampleOnePageRoutingModule } from './example-one-routing.module';

import { ExampleOnePage } from './example-one.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ExampleOnePageRoutingModule
  ],
  declarations: [ExampleOnePage]
})
export class ExampleOnePageModule {}
