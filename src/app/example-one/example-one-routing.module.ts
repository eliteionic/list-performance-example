import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExampleOnePage } from './example-one.page';

const routes: Routes = [
  {
    path: '',
    component: ExampleOnePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExampleOnePageRoutingModule {}
