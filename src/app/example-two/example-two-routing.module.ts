import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExampleTwoPage } from './example-two.page';

const routes: Routes = [
  {
    path: '',
    component: ExampleTwoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExampleTwoPageRoutingModule {}
