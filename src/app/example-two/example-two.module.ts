import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ExampleTwoPageRoutingModule } from './example-two-routing.module';

import { ExampleTwoPage } from './example-two.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ExampleTwoPageRoutingModule
  ],
  declarations: [ExampleTwoPage]
})
export class ExampleTwoPageModule {}
