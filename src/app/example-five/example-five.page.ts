import { Component, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import {
  IonContent,
  IonList,
  createAnimation,
  Animation,
} from '@Ionic/angular';

@Component({
  selector: 'app-example-five',
  templateUrl: './example-five.page.html',
  styleUrls: ['./example-five.page.scss'],
})
export class ExampleFivePage implements AfterViewInit {
  @ViewChild(IonContent, { static: false }) contentArea: IonContent;
  @ViewChild(IonList, { static: false, read: ElementRef }) list: ElementRef;

  public items: any[] = new Array(100).fill(1);
  public transitioning = false;
  public page = '1';
  private fadeAnimation: Animation;

  constructor() {}

  ngAfterViewInit() {
    this.fadeAnimation = createAnimation()
      .addElement(this.list.nativeElement)
      .fromTo('opacity', '1', '0')
      .easing('ease-in-out');
  }

  // Simulate getting page date
  async setPage(ev) {
    this.transitioning = true;
    this.page = ev.detail.value;

    // Fade the list so the data popping out/in doesn't look weird
    this.fadeAnimation.duration(200);
    this.fadeAnimation.direction('normal');
    this.fadeAnimation.play();

    await this.contentArea.scrollToTop(200);
    this.items = new Array(100).fill(this.page);

    // Bring the list back after the new data has been loaded in
    this.fadeAnimation.duration(500);
    this.fadeAnimation.direction('reverse');
    this.fadeAnimation.play();
    this.transitioning = false;
  }
}
