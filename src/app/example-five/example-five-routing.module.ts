import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExampleFivePage } from './example-five.page';

const routes: Routes = [
  {
    path: '',
    component: ExampleFivePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExampleFivePageRoutingModule {}
