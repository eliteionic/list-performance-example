import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ExampleFivePageRoutingModule } from './example-five-routing.module';

import { ExampleFivePage } from './example-five.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ExampleFivePageRoutingModule
  ],
  declarations: [ExampleFivePage]
})
export class ExampleFivePageModule {}
