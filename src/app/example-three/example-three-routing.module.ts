import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExampleThreePage } from './example-three.page';

const routes: Routes = [
  {
    path: '',
    component: ExampleThreePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExampleThreePageRoutingModule {}
