import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ExampleThreePageRoutingModule } from './example-three-routing.module';

import { ExampleThreePage } from './example-three.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ExampleThreePageRoutingModule
  ],
  declarations: [ExampleThreePage]
})
export class ExampleThreePageModule {}
