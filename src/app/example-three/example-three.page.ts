import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-example-three',
  templateUrl: './example-three.page.html',
  styleUrls: ['./example-three.page.scss'],
})
export class ExampleThreePage implements OnInit {
  public items: any[] = new Array(10000);

  constructor() {}

  ngOnInit() {}
}
