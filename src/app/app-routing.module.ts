import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'example-one',
    loadChildren: () => import('./example-one/example-one.module').then( m => m.ExampleOnePageModule)
  },
  {
    path: 'example-two',
    loadChildren: () => import('./example-two/example-two.module').then( m => m.ExampleTwoPageModule)
  },
  {
    path: 'example-three',
    loadChildren: () => import('./example-three/example-three.module').then( m => m.ExampleThreePageModule)
  },
  {
    path: 'example-four',
    loadChildren: () => import('./example-four/example-four.module').then( m => m.ExampleFourPageModule)
  },
  {
    path: 'example-five',
    loadChildren: () => import('./example-five/example-five.module').then( m => m.ExampleFivePageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
