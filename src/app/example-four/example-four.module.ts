import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ExampleFourPageRoutingModule } from './example-four-routing.module';

import { ExampleFourPage } from './example-four.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ExampleFourPageRoutingModule
  ],
  declarations: [ExampleFourPage]
})
export class ExampleFourPageModule {}
