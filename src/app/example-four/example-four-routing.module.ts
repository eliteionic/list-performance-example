import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExampleFourPage } from './example-four.page';

const routes: Routes = [
  {
    path: '',
    component: ExampleFourPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExampleFourPageRoutingModule {}
