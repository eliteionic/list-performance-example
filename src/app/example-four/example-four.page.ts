import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-example-four',
  templateUrl: './example-four.page.html',
  styleUrls: ['./example-four.page.scss'],
})
export class ExampleFourPage implements OnInit {
  public items: any[] = new Array(30);

  constructor() {}

  ngOnInit() {}

  doInfinite(event) {
    // Simulate loading data in slowly
    setTimeout(() => {
      // Push 20 more items into the array
      const extraItems = new Array(30);
      this.items.push(...extraItems);

      // Indicate loading has finished
      event.target.complete();

      // Set to true to "finish" infinite loading
      const allDataLoaded = false;

      if (allDataLoaded) {
        event.target.disabled = true;
      }
    }, 300);
  }
}
